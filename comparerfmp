from deepface import DeepFace
from deepface.commons import functions, realtime, distance as dst
import matplotlib.pyplot as plt
import cv2
import glob
import numpy as np
import os
import tensorflow as tf 
import mediapipe as mp
from retinaface import RetinaFace
import xlsxwriter
import time

#for deepface
backends = ['opencv', 'ssd', 'Dlib', 'mtcnn', 'retinaface', 'mediapipe']
backend = backends[3]

#for mediapipe
mp_face_detection = mp.solutions.face_detection 
mp_drawing = mp.solutions.drawing_utils
mp_face_mesh = mp.solutions.face_mesh
mp_drawing_styles = mp.solutions.drawing_styles
fd = mp_face_detection.FaceDetection(min_detection_confidence=0.4, model_selection=1)
fm = mp_face_mesh.FaceMesh(
    static_image_mode=True,
    max_num_faces=3,
    refine_landmarks=True,
    min_detection_confidence=0.5)


#*******smaller sample
# file = "/home/beyondem/Documents/comparing_models/*.jpg"
# file = '/home/beyondem/Documents/comparing_models/retinadetected3/*.jpg'

#*******bigger file
file = "/home/beyondem/Documents/comparing_models/images/WIDER_train/**/*.png"
pr_file = open("log.txt", "w")


#****for retina
# path1 = '/home/beyondem/Documents/comparing_models/retinadetected3'
# path2 = '/home/beyondem/Documents/comparing_models/retinadetected3/data'

#****for mp
path1 = '/home/beyondem/Documents/comparing_models/withmp'
path11 = '/home/beyondem/Documents/comparing_models/withmp/mp'
path12 = '/home/beyondem/Documents/comparing_models/withmp/rf'
path13 = '/home/beyondem/Documents/comparing_models/withmp/fm'
path2 = '/home/beyondem/Documents/comparing_models/withmp/data'


#get images
images = glob.glob(file, recursive=True)
print(len(images))


#************ Create an new Excel file and add a worksheet.
#>>>>>>>>     all.xlsx is for all models compared together
 
workbook = xlsxwriter.Workbook(os.path.join(path2,'all.xlsx'))
worksheet = workbook.add_worksheet()

# Widen the first column to make the text clearer.
worksheet.set_column('A:A', 60)
worksheet.set_column('B:B', 20)
worksheet.set_column('C:C', 20)
worksheet.set_column('D:D', 10)
worksheet.set_column('E:E', 10)
worksheet.set_column('F:F', 10)
worksheet.set_column('G:G', 10)
# Add a bold format to use to highlight cells.
bold = workbook.add_format({'bold': True})


worksheet.write(0,0, "file name")
worksheet.write(0,1, "#boxes_retina")
worksheet.write(0,2, "#boxes_mp")
worksheet.write(0,3, "#time_retina")
worksheet.write(0,4, "#time_mp")
worksheet.write(0,5, "rf_average score")
worksheet.write(0,6, "mp_average score")
i = 1


# For static images:
IMAGE_FILES = []
drawing_spec = mp_drawing.DrawingSpec(thickness=1, circle_radius=1)

with fm as face_mesh:
    with fd as face_detection:
        for f in images:
            print(f, file=pr_file)
            image = cv2.imread(f)

            #for mediapipe
            start_mp = time.time()
            imagergb =  cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            imagergb2 =  cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            obj_mp = face_detection.process(imagergb)
            results = face_mesh.process(imagergb2)
            # obj_mp = face_detection.process(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
            # results = face_mesh.process(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
            # print(obj_mp)
            end_mp = time.time()

        
            #for retinaface
            start_r = time.time()
            obj_ret = RetinaFace.detect_faces(f)
            end_r = time.time()


            # Write number of faces, with row/column notation.
            worksheet.write(i, 0, os.path.join(os.path.basename(os.path.dirname(f)), os.path.basename(f)))
            worksheet.write(i, 1, len(obj_ret))
            # if type(obj_mp) is dict:
            # if obj_mp.detections is not None:
            #     worksheet.write(i, 2, len(obj_mp.detections)*len(obj_mp.detections))
            # else:
            #     worksheet.write(i, 2, 0)

            #****************************************************************
            rf_average_score = 0.0
            #for drawing boxes using retinaface
            if type(obj_ret) is dict:
                for key in obj_ret.keys():
                    # print((obj_ret[key])['score'])
                    rf_average_score += (obj_ret[key])['score']
                    # print(key, file = pr_file)
                    identity = obj_ret[key]
                    facial_area = identity["facial_area"]
                    # worksheet.write(i, 2, facial_area[0])
                    # worksheet.write(i, 3, facial_area[1])
                    # worksheet.write(i, 4, facial_area[2])
                    # worksheet.write(i, 5, facial_area[3])
                    cv2.rectangle(image, (facial_area[2],facial_area[3]),(facial_area[0],facial_area[1]),(0,2,255),5)
                rf_average_score = rf_average_score/ len(obj_ret)
                # print(rf_average_score)
                cv2.imwrite( os.path.join(path12 ,os.path.basename(f)), image)
                cv2.waitKey(0)
            
            
            
            #***********************************************************
            #face mesh 
            annotated_image = imagergb2.copy()
            if results.multi_face_landmarks is not None:
                for face_landmarks in results.multi_face_landmarks:
                    print('face_landmarks:', face_landmarks,file=pr_file)
                    mp_drawing.draw_landmarks(
                        image=annotated_image,
                        landmark_list=face_landmarks,
                        connections=mp_face_mesh.FACEMESH_TESSELATION,
                        landmark_drawing_spec=None,
                        connection_drawing_spec=mp_drawing_styles
                        .get_default_face_mesh_tesselation_style())
                    mp_drawing.draw_landmarks(
                        image=annotated_image,
                        landmark_list=face_landmarks,
                        connections=mp_face_mesh.FACEMESH_CONTOURS,
                        landmark_drawing_spec=None,
                        connection_drawing_spec=mp_drawing_styles
                        .get_default_face_mesh_contours_style())
                    mp_drawing.draw_landmarks(
                        image=annotated_image,
                        landmark_list=face_landmarks,
                        connections=mp_face_mesh.FACEMESH_IRISES,
                        landmark_drawing_spec=None,
                        connection_drawing_spec=mp_drawing_styles
                        .get_default_face_mesh_iris_connections_style())
            annotated_image = cv2.cvtColor(annotated_image, cv2.COLOR_RGB2BGR)
            cv2.imwrite( os.path.join(path13 ,os.path.basename(f)), annotated_image)
            #***********************************************************************************
            
            
            #for displaying the images with the box
            # Draw face detections of each face.
            if not obj_mp.detections:
                continue
            image.flags.writeable = True
            # image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            counter = 0
            mp_average_score = 0.0
            # print(obj_mp.detections, file=pr_file)
            if obj_mp.detections is not None:
                # print((enumerate(obj_mp.detections)))
                for counter, detection in enumerate(obj_mp.detections):
                    score = detection.score
                    box = detection.location_data.relative_bounding_box
                    # print(score,file=pr_file)
                    # print(box,file=pr_file)
                    counter += 1
                    mp_average_score = mp_average_score + score[0]
                
                    
                    mp_drawing.draw_detection(imagergb, detection)
            mp_average_score = mp_average_score/counter
            print(counter, mp_average_score, file=pr_file)

            imagergb= cv2.cvtColor(imagergb, cv2.COLOR_RGB2BGR)
            cv2.imwrite( os.path.join(path11 ,os.path.basename(f)), imagergb)
            cv2.waitKey(0)
            # #**************************************************************
            # print(counter)
            worksheet.write(i, 2, counter)
            worksheet.write(i, 3, end_r-start_r)
            worksheet.write(i, 4, end_mp-start_mp)
            worksheet.write(i, 5, rf_average_score)
            worksheet.write(i, 6, mp_average_score)
            i = i+1             


workbook.close()

pr_file.close()
