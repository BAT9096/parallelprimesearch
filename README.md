# HelloMP
A modification of the HelloMP parallelization by Prof. J.Behrens.

**Finding the primes between 2 and a big number by using parallelization**

## Description and Purpose

This O(1)-line code demonstrates parallelization with OpenMP
OMP_NUM_THREADS represent the number of processors. For this project, I have worked on my personal computer, 
with 7 processors, and the university's GPU server with 16 processors(OMP_NUM_THREADS).


## Contents of this Project

```
README    - this file
Makefile  - Makefile tested for macOS
Hello.c   - parallelized "Hello World" C-program and an algorithm to check whether a number is prime or not by dividing the range
            to the number of processors available.
```

## What do do here

**First**, build the executable by typing

```
%> make
```

If this fails, you have to adjust your path and settings, however,
I don't know how...

**Second**, set up the number of threads (e.g. to four)

```
%> export OMP_NUM_THREADS=4
```

**Third**, run the program

```
%> Hello
```
## Result
For the prime numbers between 2 and 800,000:
```
    2:36 Seconds for seriel only(with out parallelization)
    1:16 seconds for 7 processors personall computer
    55 seconds for 16 processors(Uni-server)
```
## Framework for code, directly copied from    
(c) 2021, Jörn Behrens (joern.behrens@uni-hamburg.de)
## Added work by:
(c) 2021, Dawit Hailu (dawit.hailu@uni-hamburg.de)
