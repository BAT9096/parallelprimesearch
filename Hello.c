/*****************************************************************

 NAME:
	Hello

 Description:
	this is about the simplest program thinkable to demonstrate 
	parallel execution with OpenMP

 VERSION(S):
	1. original version		j. behrens	6/2021
    2. Edited version for parallelizing D.Hailu 7/2021

*****************************************************************/

#include <stdio.h>
#include <omp.h>


int main(int argc, char** argv)
{


/* local declarations */
	int thrd_id;

/* this is the main print statement parallelized! */

#pragma omp parallel private(thrd_id)
    {
        thrd_id = omp_get_thread_num();
        printf("Hello from process Number %d\n", thrd_id);
        int i,j;
        int count=100000;
        for(i=thrd_id*count+2;i<count*(thrd_id+1)+2;i++){
          
            for (j = 2; j<=i; j++)
            {
                if(i%j==0)
                {
                    break;
                }
            }
            if(i==j){
                printf("%d is a prime \n,",i);
            }
        }
    }

    return 0;
} /* end of main */
